﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture_Management.data;

internal class NguoiLaoDong
{
    protected string HoTen { get; set; }
    protected string NamSinh { get; set; }
    protected float LuongCoBan { get; set; }

    public NguoiLaoDong() { }

    public NguoiLaoDong(string hoten, string namsinh, float luongcoban)
    {
        NhapThongTin(hoten, namsinh, luongcoban);
    }

    public void NhapThongTin(string hoten, string namsinh, float luongcoban)
    {
        HoTen = hoten;
        NamSinh = namsinh;
        LuongCoBan = luongcoban;
    }

    public float TinhLuong()
        => LuongCoBan;

    public void XuatThongTin()
        => Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
}
