﻿using Lecture_Management.data;
using System.Text.RegularExpressions;

namespace Lecture_Managemen_OOP
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int n;
                Console.Write("Nhap so phan tu cua mang Giao vien: ");
                n = Int32.Parse(Console.ReadLine());

                // 1. tao list / array voi so luong phan tu nhap tu ban phim
                GiaoVien[] giaovien = new GiaoVien[n];

                // 2. nhap thong tin cho tung phan tu voi thong tin tu ban phim
                String hoten, namsinh, temp;
                float luongcoban, hesoluong;
                Console.WriteLine("Nhap so thong tin cho cac giao vien");
                for (int i = 0; i < giaovien.Length; i++)
                {
                    Console.WriteLine($"\nGiao vien thu {i+1} (ho ten, nam sinh, luong co ban, he so luong) ...");

                    Console.Write("Ho ten: ");
                    hoten = Console.ReadLine();
                    
                    Console.Write("Nam sinh: ");
                    namsinh = Regex.IsMatch(temp = Console.ReadLine(),"\\d{4}")? temp : throw new Exception("Wrong year format (must be 4 digit number).");
                    
                    Console.Write("Luong co ban: ");
                    luongcoban = float.Parse(Console.ReadLine());
                    
                    Console.Write("He so luong: ");
                    hesoluong = float.Parse(Console.ReadLine());
                    
                    giaovien[i] = new GiaoVien(hoten, namsinh, luongcoban, hesoluong);
                }

                // 3. hien thi thong tin cua giao vien co luong thap nhat
                float minLuong = giaovien[0].TinhLuong();
                GiaoVien minLuongGV = giaovien[0];
                foreach (GiaoVien var in giaovien)
                {
                    if(var.TinhLuong() < minLuong)
                    {
                        minLuong = var.TinhLuong();
                        minLuongGV = var;
                    }
                }
                Console.WriteLine("\nGiao vien co luong thap nhat la: ");
                minLuongGV.XuatThongTin();
                Console.WriteLine($"Luong: {minLuong}");
                    
                // 4. dung man hinh de xem ket 
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
