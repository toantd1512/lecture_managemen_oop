﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture_Management.data;

internal class GiaoVien : NguoiLaoDong
{
    protected float HeSoLuong { get; set; }

    public GiaoVien() : base() { }
    public GiaoVien(string hoten, string namsinh, float luongcoban, float hesoluong) : base(hoten, namsinh, luongcoban)
    {
        HeSoLuong = hesoluong;
    }

    public void NhapThongTin(float hesoluong)
        => HeSoLuong = hesoluong;
    public float TinhLuong()
        => LuongCoBan * HeSoLuong * 1.25f;

    public void XuatThongTin()
        => Console.WriteLine(
            $"Ho ten: {HoTen}\n" +
            $"Nam sinh: {NamSinh}\n" +
            $"Luong co ban: {LuongCoBan}\n" +
            $"He so luong: {HeSoLuong}"
            );

    public void Xuly()
        => HeSoLuong += 0.6f;


}
